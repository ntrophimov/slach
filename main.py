import time

from bs4 import BeautifulSoup
import requests
import slack
import slack.chat
import sqlite3


SLACK_API_TOKEN = 'SLACK_API_TOKEN'


def main():
    db_name = 'sent_posts.sqlite'
    with sqlite3.connect(db_name) as db_conn:
        db_cursor = db_conn.cursor()

        db_cursor.execute('''CREATE TABLE IF NOT EXISTS sent_posts
                 (post_num INTEGER PRIMARY KEY)''')
        db_conn.commit()

        while True:
            b_html = requests.get('https://2ch.hk/b/').content
            b_soup = BeautifulSoup(b_html)
            b_op_posts = b_soup.find_all(class_='post oppost')
            for b_op_post in b_op_posts:
                post_num = b_op_post['data-num']

                db_cursor.execute('SELECT * FROM sent_posts WHERE post_num = ?', (post_num,))
                post_already_sent = db_cursor.fetchone()
                if post_already_sent:
                    continue

                op_post_img = 'https://2ch.hk' + b_op_post.find('a', class_='desktop')['href']

                post_message = b_op_post.find(class_='post-message').text

                thread_url = 'https://2ch.hk/b/res/{0}.html'.format(post_num)

                slack.api_token = SLACK_API_TOKEN
                slack.chat.post_message('2ch', thread_url + ' >> ' + op_post_img + post_message, username='slach', icon_url='http://lurkmore.so/images/thumb/5/54/Peka_namekaet.jpg/150px-Peka_namekaet.jpg')

                db_cursor.execute('INSERT INTO sent_posts VALUES (?)', (post_num,))
                db_conn.commit()

            time.sleep(1)


if __name__ == "__main__":
    main()